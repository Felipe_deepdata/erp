function ClientsCommissionsController (
	$scope, $filter, $timeout, $modal,
	DTOptionsBuilder, DTColumnDefBuilder,
	Clients, Commissions,
	comissions
) {

	$scope.data = {
		'commissions': comissions,
		'filteredCommissions': comissions,
		'searchCommissions': {
			'is_active': undefined,
			'paid_at': undefined,
			'client': {
				'level': undefined
			}
		},
		'searchCommissionsForm': {
			//'from': new Date(2016,05,01),
			//'to': new Date(2016,05,30),
			'from': null,
			'to': null,
			'client': null,
			'datepicker': {
				'from': {
					'opened': false
				},
				'to': {
					'opened': false
				}
			},
			'typeahead': {
				'loading': false,
				'no_results': false
			},
			'loading': false
		},
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {
	            	extend: 'pdf', 
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'PDF',
                    title: 'Comisiones',
                    customize: function(doc, config) {

                    	//console.log(doc.content);
                    	//console.log(doc.styles);
                    	doc.pageOrientation = 'landscape';


                    	doc.content = [
                    		{
							  "text":"Comisiones",
							  "style":"title",
							  "margin":[0,0,0,12]
							},
						  	{
							    "table": {
							    	widths: [100, 150, 90, '*'],
							      	"body": (function(){

							      		var new_body = [];
							      		var header = doc.content[1].table.body[0];
							      		var clients = doc.content[1].table.body.splice(1);

							      		new_body.push(header);

							      		for(var i in clients) {

							      			new_body.push([
								      			{
								      				"text": $scope.data.commissions[i].client.name + '\n' + $scope.data.commissions[i].client.level,
								      				"bold": true,
								      				"fontSize": 11
								      			},
								      			{
								      				"text": clients[i][1].text
								      			},
								      			{
								      				"text": clients[i][2].text
								      			},
								      			{
								      				"table": {
								      					widths: [50, '*', '*', '*', '*'],
								      					"body": (function() {

								      						var body = [];

									      					var _header = [
								      							"NIVEL",
								      							"$ por referido",
								      							"# referidos",
								      							"Total referidos",
								      							"Total a pagar"
								      						];

									      					body.push(_header);

								      						if(
								      							$scope.data.commissions[i].client.level === 'MARINERO' || 
								      							$scope.data.commissions[i].client.level === 'CABO' || 
								      							$scope.data.commissions[i].client.level === 'CAPITAN' || 
								      							$scope.data.commissions[i].client.level === 'ALMIRANTE'
							      							) {
								      							body.push([
								      								"NIVEL 1",
								      								$filter('currency')(30, "$", 2),
								      								$scope.data.commissions[i].level_1,
								      								$filter('currency')(30 * $scope.data.commissions[i].level_1, "$", 2),
								      								""
							      								]);
							      							}
							      							if(
								      							$scope.data.commissions[i].client.level === 'CABO' || 
								      							$scope.data.commissions[i].client.level === 'CAPITAN' || 
								      							$scope.data.commissions[i].client.level === 'ALMIRANTE'
							      							) {
								      							body.push([
								      								"NIVEL 2",
								      								$filter('currency')(15, "$", 2),
								      								$scope.data.commissions[i].level_2,
								      								$filter('currency')(15 * $scope.data.commissions[i].level_2, "$", 2),
								      								""
							      								]);
							      							}
							      							if(
								      							$scope.data.commissions[i].client.level === 'CAPITAN' || 
								      							$scope.data.commissions[i].client.level === 'ALMIRANTE'
							      							) {
								      							body.push([
								      								"NIVEL 3",
								      								$filter('currency')(10, "$", 2),
								      								$scope.data.commissions[i].level_3,
								      								$filter('currency')(10 * $scope.data.commissions[i].level_3, "$", 2),
								      								""
							      								]);
							      							}
							      							if(
								      							$scope.data.commissions[i].client.level === 'ALMIRANTE'
							      							) {
								      							body.push([
								      								"NIVEL 4",
								      								$filter('currency')(5, "$", 2),
								      								$scope.data.commissions[i].level_4,
								      								$filter('currency')(5 * $scope.data.commissions[i].level_4, "$", 2),
								      								""
							      								]);
							      							}
							      							body.push([
							      								"",
							      								"",
							      								"",
							      								"",
							      								$filter('currency')($scope.data.commissions[i].amount, "$", 2)
						      								]);

									      					return body;
									      				})(),
									      				"headerRows": 1
								      				},
								      				"layout": 'lightHorizontalLines',
								      				"headerRows": 1
								      			}
							      			])

							      		}
							      		return new_body;

							      	})(),
							      	"headerRows": 1
							    }
						  	}
						];

						//console.log(doc.content);
                    	config.download = 'open';
                    }
	            },
	            {
	            	extend: 'print',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Imprimir',
                    title: 'Comisiones',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ])
            .withOption('order', []),
		'dtColumnDefs': [
			DTColumnDefBuilder.newColumnDef(2).withOption("type", "date-nl"),
            DTColumnDefBuilder.newColumnDef(3).notSortable(),
	        DTColumnDefBuilder.newColumnDef(4).notSortable()
	    ],
	    'dtInstance': {}
	}

    $scope.open = function ($event, datepicker) {

        $event.preventDefault();
        $event.stopPropagation();
        $scope
        	.data
        	.searchCommissionsForm
        	.datepicker[datepicker].opened = 
    	$scope
	    	.data
	    	.searchCommissionsForm
	    	.datepicker[datepicker] = {
	    		"opened" : !$scope.data.searchCommissionsForm.datepicker[datepicker].opened
	    	};

    }

    $scope.getSalesAgent = function (name) {

    	return Clients
    		.getAll({
    			'name': name,
    			'role': 'Sales Agent'
    		})
    		.then(function(response) {
    			return response;
    		})
    
    }

    $scope.getCommissions = function (form, criteria) {

    	if(form.$valid) {

    		$scope.data.searchCommissionsForm.loading = true;

    		Commissions
    			.getAll((function(){

    				var _criteria = {
	    				'from': $filter('date')(criteria.from, 'yyyy-MM-dd'),
	    				'to': $filter('date')(criteria.to, 'yyyy-MM-dd'),
	    			}

	    			if(angular.isObject(criteria.client)) {
	    				_criteria.id_clients = criteria.client.id_clients;
	    			}

	    			return _criteria;

    			})())
    			.then(function(commissions) {

    				$scope.data.searchCommissionsForm.loading = false;
    				$scope.data.commissions = commissions;
    				$scope.filterCommissions(commissions);

    			})

    	} else {

			$scope.$broadcast('show-errors-check-validity');

            swal({
                title: 'Un momento',
                text: "Favor de completar los datos obligatorios.",   
                type: "error",    
                confirmButtonColor: "#128f76",   
                confirmButtonText: "Aceptar"
            });
    	}

    }

    $scope.registerAsPaid = function (commission) {


        swal({   
            title: "Atención",   
            text: "¿ Estás seguro de marcar la comisión como pagada ?",   
            type: "warning",   
            showCancelButton: true,   
            closeOnConfirm: false,
            confirmButtonText: "Continuar",
            confirmButtonColor: "#23c6c8",
            cancelButtonText: "Cancelar",
            animation: "slide-from-top",   
        }, function (){   

        	commission.loading = true;

            Commissions
                .registerAsPaid({
                    'id_commissions': commission.id_commissions
                })
                .then(function (response) {

                    commission.loading = false;
                    commission.paid_at = response.paid_at;

                    swal({   
                        title: "Operación realizada correctamente",   
                        text: "",   
                        type: "success",       
                        confirmButtonText: "Continuar",
                        confirmButtonColor: "#2196F3",
                        allowOutsideClick: true
                    });

                });

        });

	}
	
	$scope.showDeclined = function (commission) {
		$modal.open({

			templateUrl: 'views/clients-commissions-declined.html',
			controllerAs: 'instance',
			size: 'lg',
			backdrop: 'static',
			keyboard: false,
			resolve: {
				commissionData: function () {
					return Commissions
						.getDeclined({
							'id_commissions': commission.id_commissions
						});
				}
			},
			controller: [
				'$filter', '$timeout', '$modalInstance',
				'commissionData',

				function (
					$filter, $timeout, $modalInstance,
					commissionData) {

					var instance = this;

					instance.data = {
						'commission': commissionData,
						'loading': false
					}
					
					instance.close = function () {
						$modalInstance.dismiss();
					}
				}
			]
		})
	}

    $scope.filterCommissions = function(commissions) {
        $timeout(function(){
            $scope.data.filteredCommissions = $filter('filter')(commissions, $scope.data.searchCommissions);
        });
    
    }

}
ClientsCommissionsController.$inject = [
	'$scope', '$filter', '$timeout', '$modal',
	'DTOptionsBuilder', 'DTColumnDefBuilder',
	'Clients', 'Commissions',
	'comissions'
];