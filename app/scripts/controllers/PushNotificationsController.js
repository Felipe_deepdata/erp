function PushNotificationsController(
	$scope, PushNotifications, Users, groups, roles) {
	$scope.data = {
		'name': '',
		'users': [],
		'groups': groups,
		'roles': roles,
		'selectedUsers': [],
		'selectedGroups': [],
		'selectedRoles': [],
		'message': ''
	};
	$scope.searchUser = function(name) {
		Users.get({'name': name}).then(function(users) {
			$scope.data.users = users;
		});
	}
	$scope.selectUser = function(user) {
		if ($scope.data.selectedUsers.indexOf(user) == -1)
			$scope.data.selectedUsers.push(user);
	}
	$scope.removeUser = function(user) {
		$scope.data.selectedUsers.splice($scope.data.selectedUsers.indexOf(user), 1);
	}
	$scope.selectGroup = function(group) {
		if ($scope.data.selectedGroups.indexOf(group) == -1)
			$scope.data.selectedGroups.push(group);
	}
	$scope.removeGroup = function(group) {
		$scope.data.selectedGroups.splice($scope.data.selectedGroups.indexOf(group), 1);
	}
	$scope.selectRole = function(role) {
		if ($scope.data.selectedRoles.indexOf(role) == -1)
			$scope.data.selectedRoles.push(role);
	}
	$scope.removeRole = function(role) {
		$scope.data.selectedRoles.splice($scope.data.selectedRoles.indexOf(role), 1);
	}
	$scope.notifyUser = function(message, users) {
		PushNotifications
			.notifyUser(message, users.map(function(user) { return user.id; }))
			.then(function() {
				swal({
					title: 'Notificacion enviada',
					text: '',
					type: 'success',
					confirmButtonColor: '#128f76',
					confirmButtonText: 'Aceptar'
				}, function() {
					$scope.data.selectedUsers = [];
				});
			});
	}
	$scope.notifyGroup = function(message, groups) {
		PushNotifications
			.notifyGroup(message, groups.map(function(group) { return group.id_groups; }))
			.then(function() {
				swal({
					title: 'Notificacion enviada',
					text: '',
					type: 'success',
					confirmButtonColor: '#128f76',
					confirmButtonText: 'Aceptar'
				}, function() {
					$scope.data.selectedGroups = [];
				});
			});
	}
	$scope.notifyRole = function(message, roles) {
		PushNotifications
			.notifyGroup(message, roles.map(function(role) { return role.id; }))
			.then(function() {
				swal({
					title: 'Notificacion enviada',
					text: '',
					type: 'success',
					confirmButtonColor: '#128f76',
					confirmButtonText: 'Aceptar'
				}, function() {
					$scope.data.selectedRoles = [];
				});
			});
	}
}
PushNotificationsController.$inject = [
	'$scope', 'PushNotifications', 'Users', 'groups', 'roles'
];
