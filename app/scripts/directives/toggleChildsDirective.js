function toggleChilds() {
    return {
        link: function($scope, el, attrs){
            $(el).on("click", ".showChilds", function(){

                var parent = $(this).parents('tr');

                if($(parent).next().hasClass('childs')) {

                    $(parent).next().addClass('hidden').insertAfter($(parent).find('td:last'));

                } else {

                    $(parent).find('.childs').removeClass('hidden').insertAfter(parent);
                
                }

            });
        }
        
    }
}