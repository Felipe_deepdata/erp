function panzoom() {
    return function(scope, element, attrs) {

        var panzoom = $(element).panzoom({
            increment: 0.2,
            minScale: 0.5,
            maxScale: 5,
        });

        panzoom.parent().on('mousewheel.focal', function( e ) {
            e.preventDefault();
            var delta = e.delta || e.originalEvent.wheelDelta;
            var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
            panzoom.panzoom('zoom', zoomOut, {
                increment: 0.1,
                animate: false,
                focal: e
            });
        });
    }
}
