function xforydays() {
    return {
        require: 'ngModel',
        link: function (scope, elem, attr, ngModel) {

            scope.$watch(attr.xforydays, function (newValue, oldValue) {
                console.log(newValue);
                console.log(attr.min);
                if (newValue > 3) {
                    attr.min = 0;
                } else {
                    attr.min = 3;
                }

                if (newValue < 1 && scope.instance.data.group.deposit < 3) {
                    valid = false;
                } else {
                    valid = true;
                }
                ngModel.$setValidity('xforydays', valid);
            });

            ngModel.$parsers.unshift(function (value) {
                var valid = true;
                
                if (scope.instance.data.group.trial_days_price < 1 && value < 3) {
                    valid = false;
                }
                ngModel.$setValidity('xforydays', valid);
                return value;
            }); 
            
            
        }
    };
}
