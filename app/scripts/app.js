'use strict';

angular
    .module('aguagente-front', [
        'ngAnimate',
        'ngResource',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'FBAngular',
        'aguagente-front-controllers',
        'aguagente-front-services',
        'aguagente-front-directives',
        'aguagente-front-constants',
        'aguagente-front-filters',
        'datatables',
        'datatables.buttons',
        'angular-loading-bar',
        'angular-ladda',
        'angular.filter',
        'ui.tree',
        'highcharts-ng',
        'angularFileUpload',
        'watchDom',
        'NgSwitchery',
        'cgBusy',
        'ui.bootstrap.datetimepicker'
    ])
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$compileProvider', 'cfpLoadingBarProvider',

        /**
         * function app
         */
        function($stateProvider, $urlRouterProvider, $httpProvider, $compileProvider, cfpLoadingBarProvider) {

            Highcharts.setOptions({
                lang: {
                    months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    weekdays: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                    shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
                }
            });

            $httpProvider.defaults.withCredentials = true;

            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript|local|data):/);
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|data:image\//);

            $httpProvider
                .interceptors
                .push(['$q', '$injector', function($q, $injector) {
                    return {
                        responseError: function(response) {

                            if (response.status === 401) {

                                $injector.get('SessionService').destroy();
                                $injector.get('AuthService').getToken();
                                $injector.get('$state').go('login');

                                swal({
                                    title: "Sesión expirada!",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false,
                                    animation: 'slide-from-top',
                                    type: "warning",
                                    showCancelButton: false,
                                    confirmButtonText: "Continuar",
                                    closeOnConfirm: true
                                });

                            } else if (/subscribeToPlan/.test(response.config.url)) {

                                switch (response.status) {

                                    case 402:
                                        swal({
                                            title: 'Error en la solicitud',
                                            text: 'La tarjeta ha sido declinada o hay fondos insuficientes. (' + response.data.response + ')',
                                            type: "error",
                                            confirmButtonColor: "#128f76",
                                            confirmButtonText: "Aceptar"
                                        });
                                        break;
                                    case 500:

                                        swal({
                                            title: "Ocurrió un error durante la solicitud",
                                            text: "",
                                            type: "error",
                                            confirmButtonText: "Continuar",
                                            allowOutsideClick: true
                                        });

                                        break;

                                }

                            } else if (response.status === 404) {

                                $injector.get('$state').go('error-404');

                            } else {
                                console.log(response.status);
                                swal({
                                    title: 'Error en la solicitud',
                                    text: "verifica los datos<br/>" + (function() {
                                        var message = '';

                                        for (var i in response.data.response.errors)
                                            message += response.data.response.errors[i].join("<br/>") + "<br/>";

                                        return message;
                                    })(),
                                    type: "error",
                                    confirmButtonColor: "#128f76",
                                    confirmButtonText: "Aceptar",
                                    html: true
                                });

                            }

                            return $q.reject(response);

                        }
                    }
                }]);

            $urlRouterProvider.otherwise('/login');

            $stateProvider
                .state('login', {
                    url: '/login',
                    templateUrl: 'views/login.html',
                    controller: 'LoginController'
                })
                .state('forgot-password', {
                    url: '/forgot-password',
                    templateUrl: 'views/forgot-password.html',
                    controller: 'ForgotPasswordController'
                })
                .state('reset-password', {
                    url: '/reset-password',
                    templateUrl: 'views/reset-password.html',
                    controller: 'ResetPasswordController'
                })
                .state('ticket-complete', {
                    url: '/ticket-complete',
                    templateUrl: 'views/error-tickets-complete.html',
                    controller: 'ErrorTicketsCompleteController',
                    resolve: {
                        assignation: [

                            'Tickets', '$location', '$state',

                            function(Tickets, $location, $state) {

                                return Tickets
                                    .getAssignation({
                                        'id_assignations': $location.search().id_assignations
                                    })
                                    .then(function(assignation) {
                                        if (assignation.ticket.status !== 'assigned') {
                                            //$state.go('error-404');
                                        } else {
                                            return assignation;
                                        }
                                        return assignation;
                                    });
                            }
                        ],

                        errorCodes: [

                            'ErrorCodes',

                            function(ErrorCodes) {

                                return ErrorCodes
                                    .getAll()
                                    .then(function(response) {
                                        return response;
                                    });
                            }
                        ],

                        /*position: [

                            '$q', 'Geolocation', 'AGUAGENTE_GOOGLE_API_KEY',

                            function($q, Geolocation, AGUAGENTE_GOOGLE_API_KEY) {

                                return $q(function (resolve, reject) {

                                    Geolocation
                                    .getCurrentPosition({
                                        'key': AGUAGENTE_GOOGLE_API_KEY
                                    }).then(function (position) {

                                        resolve({
                                            'position': position,
                                            '_status': true
                                        });

                                    });

                                });

                            }
                        ]*/
                        position: [

                            '$q',

                            function($q) {

                                return $q(function(resolve, reject) {

                                    navigator
                                        .geolocation
                                        .getCurrentPosition(function(position) {

                                            resolve({
                                                'position': position,
                                                '_status': true
                                            });

                                        }, function(error) {

                                            resolve({
                                                'error': error,
                                                '_status': false
                                            });

                                            // error.code can be:
                                            //   0: unknown error
                                            //   1: permission denied
                                            //   2: position unavailable (error response from location provider)
                                            //   3: timed out

                                        }, {
                                            timeout: 10 * 1000
                                        });

                                });

                            }
                        ]
                    }
                })
                .state('error-404', {
                    url: '/error-404',
                    templateUrl: 'views/404.html',
                    data: {
                        pageTitle: 'Error | 404'
                    },
                })
                .state('app', {
                    abstract: true,
                    url: '/app',
                    templateUrl: 'views/common/content.html'
                })
                .state('app.main', {
                    url: '/main',
                    controller: 'DashboardController',
                    templateUrl: 'views/main.html'
                })
                .state('app.clients', {
                    url: '/clients',
                    templateUrl: 'views/clients.html',
                    controller: 'ClientsController',
                    data: {
                        pageTitle: 'Clientes'
                    },
                    resolve: {
                        groups: ['Groups', function(Groups) {
                            return Groups.getAll();
                        }]
                    }
                })
                .state('app.clients-groups', {
                    url: '/groups',
                    templateUrl: 'views/clients-groups.html',
                    controller: 'ClientsGroupsController',
                    data: {
                        pageTitle: 'Grupos'
                    },
                    resolve: {

                        groups: ['Groups', function(Groups) {

                            return Groups
                                .getAll()
                                .then(function(response) {
                                    return response;
                                });
                        }]
                    }
                })
                .state('app.clients-commissions', {
                    url: '/commissions',
                    templateUrl: 'views/clients-commissions.html',
                    controller: 'ClientsCommissionsController',
                    data: {
                        pageTitle: 'Comisiones'
                    },
                    resolve: {
                        comissions: ['Commissions', function(Commissions) {
                            return Commissions
                                .getAll()
                                .then(function(response) {
                                    return response;
                                });
                        }]
                    }
                })
                .state('app.contracts', {
                    url: '/contracts',
                    templateUrl: 'views/contracts.html',
                    controller: 'ContractsController',
                    data: {
                        pageTitle: 'Contratos'
                    },
                    resolve: {
                        contracts: ['Contracts', function(Contracts) {
                            return Contracts
                                .getAll()
                                .then(function(response) {
                                    return response;
                                });
                        }]
                    }
                })
                .state('app.contracts-extras', {
                    url: "/contracts-extras",
                    templateUrl: 'views/contracts-extras.html',
                    controller: 'ContractsExtrasController',
                    data: {
                        pageTitle: 'Contratos - Cargos Extras'
                    },
                    resolve: {

                        categories: ['Categories', function(Categories) {

                            return Categories
                                .getAll()
                                .then(function(response) {
                                    return response;
                                });
                        }]
                    }
                })
                .state('app.administrators', {
                    url: '/administradores',
                    templateUrl: 'views/administrators.html',
                    controller: 'AdministratorsController',
                    data: {
                        pageTitle: 'Administradores'
                    },
                    resolve: {

                        administrators: ['Users', function(Users) {

                            return Users
                                .getAdministrators()
                                .then(function(response) {
                                    return response;
                                });
                        }]

                    }
                })
                .state('app.technicians', {
                    url: '/tecnicos',
                    templateUrl: 'views/technicians.html',
                    controller: 'TechniciansController',
                    data: {
                        pageTitle: 'Técnicos'
                    },
                    resolve: {

                        technicians: ['Users', function(Users) {

                            return Users
                                .getAllTechnicians()
                                .then(function(response) {
                                    return response;
                                });
                        }]

                    }
                })
                .state('app.error-codes', {
                    url: '/codigos-error',
                    templateUrl: 'views/error-codes.html',
                    controller: 'ErrorCodesController',
                    data: {
                        pageTitle: 'Códigos de error'
                    },
                    resolve: {

                        errorCodes: ['ErrorCodes', function(ErrorCodes) {

                            return ErrorCodes
                                .getAll()
                                .then(function(response) {
                                    return response;
                                });
                        }]

                    }
                })
                .state('app.tickets-assign', {
                    url: '/tickets-technicians-assign',
                    templateUrl: 'views/tickets-technicians-assign.html',
                    controller: 'AssignTechniciansTicketsController',
                    data: {
                        pageTitle: 'Asignación de tickets'
                    },
                    resolve: {

                        technicians: ['Users', function(Users) {

                            return Users
                                .getTechnicians()
                                .then(function(response) {
                                    return response;
                                });
                        }],

                        tickets: ['Tickets', function(Tickets) {

                            return Tickets
                                .getAll()
                                .then(function(response) {
                                    return response;
                                });

                        }]

                    }
                })
                .state('app.error-tickets', {
                    url: '/error-tickets?id_technicians',
                    templateUrl: 'views/error-tickets.html',
                    controller: 'ErrorTicketsController',
                    data: {
                        pageTitle: 'Seguimiento de errores'
                    },
                    resolve: {

                        errorCodes: ['ErrorCodes', function(ErrorCodes) {

                            return ErrorCodes
                                .getAll()
                                .then(function(response) {
                                    return response;
                                });

                        }],
                      technicians: ['Users', function (Users) {

                        return Users
                          .getTechnicians()
                          .then(function (response) {
                            return response;
                          });

                      }]
                    }
                })
                .state('app.polls', {
                    url: '/encuestas',
                    templateUrl: 'views/polls.html',
                    controller: 'PollsController',
                    data: {
                        pageTitle: 'Encuestas'
                    },
                    resolve: {

                        polls: ['Polls', function(Polls) {

                            return Polls
                                .getAll()
                                .then(function(response) {
                                    return response;
                                })

                        }]

                    }
                })
                .state('app.social-gallery', {
                    url: "/social-gallery",
                    templateUrl: 'views/social-gallery.html',
                    controller: 'SocialGalleryController',
                    data: {
                        pageTitle: 'Galería'
                    },
                    resolve: {

                        posts: ['Posts', function(Posts) {

                            return Posts.getAll();

                        }]
                    }
                })
                .state('app.push-notifications', {
                    url: "/push-notifications",
                    templateUrl: 'views/push-notifications.html',
                    controller: 'PushNotificationsController',
                    data: {
                        pageTitle: 'Notificaciones'
                    },
                    resolve: {
                        groups: ['Groups', function(Groups) {
                            return Groups.getAll();
                        }],
                        roles: ['Roles', function(Roles) {
                            return Roles.getAll();
                        }]
                    }
                })
                .state('app.finance-clients', {
                    url: "/finance-clients",
                    templateUrl: 'views/finance-clients.html',
                    controller: 'FinanceClientsController',
                    data: {
                        pageTitle: 'Clientes'
                    },
                    resolve: {
                        clients: ['FinanceClients', function(FinanceClients) {
                            return FinanceClients.getAll();
                        }]
                    }
                })
                .state('app.social-responsability', {
                    url: "/social-responsability",
                    templateUrl: 'views/social-responsability.html',
                    controller: 'SocialResponsabilityController',
                    data: {
                        pageTitle: 'Responsabilidad Social'
                    }
                })
                .state('app.invoices', {
                    url: "/invoices",
                    templateUrl: 'views/invoices.html',
                    controller: 'InvoiceController',
                    data: {
                        pageTitle: 'Facturas'
                    }
                })


            jQuery.extend(jQuery.fn.dataTableExt.oSort, {
                "date-nl-pre": function(date) {
                    return moment(date, "ddd DD MMM YYYY");
                },

                "date-nl-asc": function(a, b) {
                    return (a.isBefore(b) ? -1 : (a.isAfter(b) ? 1 : 0));
                },

                "date-nl-desc": function(a, b) {
                    return (a.isBefore(b) ? 1 : (a.isAfter(b) ? -1 : 0));
                },

                "date-wd-pre": function(date) {
                    return moment(date, "DD MMM YYYY");
                },

                "date-wd-asc": function(a, b) {
                    return (a.isBefore(b) ? -1 : (a.isAfter(b) ? 1 : 0));
                },

                "date-wd-desc": function(a, b) {
                    return (a.isBefore(b) ? 1 : (a.isAfter(b) ? -1 : 0));
                }
            });

        }

    ])

.run(['$rootScope', '$state', '$location', 'DTDefaultOptions', 'AuthService', 'SessionService',

    function($rootScope, $state, $location, DTDefaultOptions, AuthService, SessionService) {

        AuthService.getToken();

        $rootScope.$on('$stateChangeStart', function(event, next) {

            $rootScope.$state = $state;

            if (
                next.name === 'forgot-password' ||
                next.name === 'reset-password' ||
                next.name === 'ticket-complete' ||
                next.name === 'error-404') {

                if (
                    (next.name === 'reset-password' && $location.search().token === undefined) ||
                    (next.name === 'ticket-complete' && $location.search().id_assignations === undefined)
                ) {

                    event.preventDefault();
                    $state.transitionTo('error-404');
                }


            } else if (next.name !== 'login' && !AuthService.isAuthenticated()) {

                event.preventDefault();
                $state.transitionTo('login');

            } else if (AuthService.isAuthenticated()) {

                $rootScope.setCurrentUser(SessionService.getUser());

            }

        });

        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

            event.preventDefault();
            console.log(error);

        })

        DTDefaultOptions.setLanguage({
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        });

    }

]);
