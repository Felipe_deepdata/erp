/**
 * Commissions
 * Modela el comportamiento de las comisiones, que son un porcentaje de los pagos a cada cliente referido.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param $filter ng/filter Filters are used for formatting data displayed to the user.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, registerAsPaid: registerAsPaid}}
 * @constructor
 */
function Commissions ($resource, $q, $filter, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/commissions/:id_commissions',
		{
			id_commissions: '@id_commissions'
		},
		{
			getAll: {
				method: 'GET'
			},
			getDeclined: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/commissions/getDeclined/:id_commissions'
			},
			registerAsPaid: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/commissions/:id_commissions/registerAsPaid'
			}
		}
	)

	return  {

    /**
     * getAll
     * Devuelve todas las comisiones del cliente
     * @param params
     */
		getAll: function(params) {

			return resource
				.get(params)
				.$promise
				.then(function (response) {

					var response = response.response;

					for(var i in response) {

						response[i].deposit = parseFloat(response[i].deposit) / 100;
						response[i].installation_fee = parseFloat(response[i].installation_fee) / 100;
						response[i].monthly_fee = parseFloat(response[i].monthly_fee) / 100;

						response[i]._from_month = moment(moment.utc(response[i].calculated_at).toDate()).subtract(1., 'months').format('MMMM');
						response[i]._to_month = moment(moment.utc(response[i].calculated_at).toDate()).format('MMMM');

					}

					return response;

				});

		},

		getDeclined: function (params) {

			return resource
					.getDeclined(params)
					.$promise
					.then(function (response) {
						return response;
					});

		},

		/**
		 * registerAsPaid
		 * Se marca una comisión como pagada por medio de un ID.
		 * @param params number: id ID de comisión.
		 */
		registerAsPaid: function(params) {

			return resource
				.registerAsPaid(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		}
	};

}

Commissions.$inject = ['$resource', '$q', '$filter', 'API_AGUAGENTE'];
