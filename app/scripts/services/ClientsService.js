/**
 * Clients
 * Servicio que modela la información relacionada a los clientes.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param $filter ng/filter Filters are used for formatting data displayed to the user.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{get: get, getAll: getAll, update: update, delete: delete, setLevel: setLevel, setStatus: setStatus, getValidationHistory: getValidationHistory, getHierarchicalTree: getHierarchicalTree, subscribeToPlan: subscribeToPlan, makeSalesAgent: makeSalesAgent, switchInvoice: switchInvoice, getCharges: getCharges, refundCharge: refundCharge}}
 * @constructor
 */
function Clients ($resource, $q, $filter, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/clients/:id_clients',
		{
			id_clients: '@id_clients',
			id_charges: '@id_charges'
		},
		{
			update: {
				method: 'POST',
				//data: $.param(data),
				//isArray: true,
				headers: {
                    'Content-Type': undefined
                },
				transformRequest: function(data) {

                    var formData = new FormData();

					//formData.append($.param(data));
                    for (var key in data){

                        formData.append(key, data[key]);

                    }

                    formData.append('_method','PUT');

                    return formData;

                }
			},
			delete: {
				method: 'DELETE'
			},
			setStatus: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/clients/:id_clients/setStatus'
			},
			getRejections: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/rejections'
			},
			getInvalidations: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/invalidations'
			},
			getCancellations: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/cancellations'
			},
			getCorrections: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/corrections'
			},
			getHierarchicalTree: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/clients/:id_clients/hierarchicalTree'
			},
			subscribeToPlan: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/clients/:id_clients/subscribeToPlan'
			},
			makeSalesAgent: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/clients/:id_clients/attachRole',
				params: {
					'role': 'Sales Agent'
				}
			},
			switchInvoice : {
				method : 'POST',
				url: API_AGUAGENTE.URL + '/clients/:id_clients/switchInvoice'
			},
			getCharges: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/charges'
			},
			refundCharge: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/charges/:id_charges/refund'
			},
			setLevel: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/client/:id_clients/level'
			},
			getComissions: {
				method: 'get',
				url: API_AGUAGENTE.URL + '/commissions/getCommissions/:id_clients'
			},
			getLoosingComissions: {
				method: 'get',
				url: API_AGUAGENTE.URL + '/commissions/getLoosingCommissions/:id_clients'
			},
			subscribeToPlanOffline: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/clients/:id_clients/subscribeToPlanOffline'
			},
		}
	);

	return  {

    /**
     * get
     * Devuelve la información de un solo cliente por medio de un ID.
     * @param params
     */
		get: function(params) {

			return resource
				.get(params)
				.$promise
				.then(function (response) {

					var response = response.response;

					response.deposit = parseFloat(response.deposit) / 100;
					response.installation_fee = parseFloat(response.installation_fee) / 100;
					response.monthly_fee = parseFloat(response.monthly_fee) / 100;

					return response;

				});

		},

    /**
     * getAll
     * Devuelve la información de todos los clientes existentes en base de datos.
     * @param params
     */
		getAll: function(params) {

			return resource
				.get(params)
				.$promise
				.then(function (response) {

					var clients = response.response;

					for(var i in clients) {

						clients[i].deposit = parseFloat(clients[i].deposit) / 100;
						clients[i].installation_fee = parseFloat(clients[i].installation_fee) / 100;
						clients[i].monthly_fee = parseFloat(clients[i].monthly_fee) / 100;

					}

					return clients;

				});

		},

    /**
     * update
     * Actualiza la información relacionada a un solo cliente por medio de un ID.
     * @param params
     */
		update: function(params) {

			return resource
				.update(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * delete
     * Borra la información relacionada a un solo cliente por medio de un ID.
     * @param params
     */
		delete: function(params) {

			return resource
				.delete(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * setLevel
     * Asigna el nivel del cliente según los clientes referido que tenga.
     * @param params
     */
		setLevel: function(params) {

			return resource
				.setLevel(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * setStatus
     *  Le asigna un status al cliente por medio de un ID.
     * @param params
     */
		setStatus: function(params) {

			return resource
				.setStatus(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * getValidationHistory
     * Obtiene el historial de validación de un cliente por medio de su ID.
     * @param params
     */
		getValidationHistory: function(params) {

			return $q.all({
				'rejections': resource
					.getRejections(params)
					.$promise
					.then(function (response) {
						return response.response;
					}),
				'invalidations': resource
					.getInvalidations(params)
					.$promise
					.then(function (response) {
						return response.response;
					}),
				'cancellations': resource
					.getCancellations(params)
					.$promise
					.then(function (response) {
						return response.response;
					}),
				'corrections': resource
					.getCorrections(params)
					.$promise
					.then(function (response) {

						for(var i in response.response) {

							response.response[i].reasons = 'Correción de datos';
						}

						return response.response;

					})
			});

		},

    /**
     * getHierarchicalTree
     * Devuelve un Arbol, del cliente, organizado por sus jerarquías.
     * @param params
     */
		getHierarchicalTree: function(params) {

			return resource
				.getHierarchicalTree(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * subscribeTuPlan
     * Genera un cargo al cliente y despues lo suscribe a un plan de CONEKTA.
     * @param params
     */
		subscribeToPlan: function(params) {

			return resource
				.subscribeToPlan(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * makeSalesAgent
     * Convierte a un cliente en Agente de ventas por medio de un ID
     * @param params
     * @returns {*|{method, url, params}|void}
     */
		makeSalesAgent: function(params) {

			return resource
				.makeSalesAgent(params);

		},

    /**
     * switchInvoice
     * Cambia el status de *require_invoice* de un cliente por medio de su ID. El valor cambia al negar su valor actual.
     * @param id ID del cliente a modificar.
     */
		switchInvoice: function(id){
			return resource
				.switchInvoice({"id_clients":id})
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * getCharges
     * Obtiene el historial de cargos del cliente por medio de su ID.
     * @param params
     */
		getCharges: function(params) {

			return resource
				.getCharges(params)
				.$promise
				.then(function (response) {

					var response = response.response;

					for(var i in response) {

						response[i].amount = parseFloat(response[i].amount) / 100;
						response[i].fee = parseFloat(response[i].fee) / 100;

						for(var p in response[i].refunds) {

							response[i].refunds[p].amount = parseFloat(response[i].refunds[p].amount) / 100;

						}

					}

					return response;

				});

		},

    /**
     * refundCharge
     * Efectúa la devolución de un cargo a un cliente.
     * @param payload
     */
		refundCharge: function(payload) {

			return resource
				.refundCharge(payload)
				.$promise
				.then(function (response) {

					var response = response.response;

					response.amount = parseFloat(response.amount) / 100;
					response.fee = parseFloat(response.fee) / 100;
					response.client.deposit = parseFloat(response.client.deposit) / 100;
					response.client.installation_fee = parseFloat(response.client.installation_fee) / 100;
					response.client.installation_fee = parseFloat(response.client.installation_fee) / 100;

					for(var p in response.refunds) {

						response.refunds[p].amount = parseFloat(response.refunds[p].amount) / 100;

					}

					return response;

				});

		},

		getComissions: function (params) {

			return resource
				.getComissions(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},
		getLoosingComissions: function (params) {

			return resource
				.getLoosingComissions(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},
		subscribeToPlanOffline: function (params) {

			return resource
				.subscribeToPlanOffline(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

	};

}

Clients.$inject = ['$resource', '$q', '$filter', 'API_AGUAGENTE'];
