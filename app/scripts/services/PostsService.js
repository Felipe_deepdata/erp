/**
 * Post
 * Funciones básicas de almacenamiento de los elementos en la sección de redes sociales.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param Imagen Servicio Imagen.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, create: create, update: update, delete: delete}}
 * @constructor
 */
function Posts ($resource, Imagen, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/posts/:id_posts',
		{
			id_posts: '@id_posts'
		},
		{
			getAll: {
				method: 'GET'
			},

			create: {
				method: 'POST',
				headers: {
					'Content-Type': undefined
				},
				transformRequest: function(data) {
					var formData = new FormData();
					if(data.title) formData.append('title', data.title);
					if(data.text) formData.append('text', data.text);
					if(data.image) {
						var blob = Imagen.base64ToBlob(data.image);
						formData.append('image', blob.file, 'image' + '.' + (blob.mime.replace('image/','')));
					}
					return formData;
				}
			},

			update: {
				method: 'POST',
				headers: {
					'Content-Type': undefined
				},
				transformRequest: function(data) {
            		var formData = new FormData();
					if(data.title) formData.append('title', data.title);
					if(data.text) formData.append('text', data.text);
					if(data.image) {
						var blob = Imagen.base64ToBlob(data.image);
						formData.append('image', blob.file, 'image' + '.' + (blob.mime.replace('image/','')));
					}
					formData.append('_method', 'PUT');
					return formData;
				}
			},

			delete: {
				method: 'DELETE'
			}
		}
	);

	return  {

    /**
     * getAll
     * Devuelve todos los Posts.
     */
		getAll: function () {

			return resource
				.getAll()
				.$promise
				.then(function(response) {
					var posts = response.response;
					for(var i in posts) {
						if(typeof posts[i].image === 'string') {
							(function(j) {
								Imagen
									.urlToBase64(posts[j].image)
									.then(function(data) {
										posts[j].image = data;
									});
							})(i);
						}
					}
					return posts;
				});

		},

    /**
     * create
     * Crea un elemento Post.
     * @param payload
     */
		create: function(payload) {

			return resource
				.create(payload)
				.$promise
				.then(function (response) {
					var post = response.response;
					if(typeof post.image === 'string') {
						return Imagen.urlToBase64(
							post.image
						).then(function(data) {
							post.image = data;
							return post;
						});
					} else {
						return post;
					}
				});

		},

    /**
     * update
     * Actualiza la información de un elemento Post por medio de su ID.
     * @param payload
     */
		update: function(payload) {

			return resource
				.update(payload)
				.$promise
				.then(function (response) {
					var post = response.response;
					if(typeof post.image === 'string') {
						return Imagen.urlToBase64(
							post.image
						).then(function(data) {
							post.image = data;
							return post;
						});
					} else {
						return post;
					}
				});

		},

    /**
     * delete
     * Elimina un elemento Post por medio de su ID.
     * @param params
     */
		delete: function(params) {

			return resource
				.delete(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		}

	};

}

Posts.$inject = ['$resource', 'Imagen', 'API_AGUAGENTE'];
