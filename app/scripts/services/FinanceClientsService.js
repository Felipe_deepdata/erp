/**
 * FinanceClientsService
 * Devuelve todos los clientes usando filtros.
 *
 * @param $http  ng/http
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll}}
 * @constructor
 */
function FinanceClientsService ($http, API_AGUAGENTE) {

	return  {

    /**
     * getAll
     * Devuelve todos los clientes usando filtros.
     * @param params objeto con información sobre el filtrado de los clientes.
     */
		getAll: function(params) {

			return $http
				.get(API_AGUAGENTE.URL + '/finance-clients', {"params": params})
				.then(function(response) {
					var clients = response.data.response;
					for(var i in clients) {
						clients[i].debt = parseInt(clients[i].debt) / 100;
					}
					return clients;
				});

		}

	};

}

FinanceClientsService.$inject = ['$http', 'API_AGUAGENTE'];
