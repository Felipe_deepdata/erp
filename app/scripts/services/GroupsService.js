/**
 * Groups
 * Modela la interacción de los grupos a los que pertenecen los clientes.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, create: create, update: update, delete: delete, associateClients: associateClients}}
 * @constructor
 */
function Groups ($resource, $q, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/groups/:id_groups',
		{
			'id_groups': '@id_groups'
		},
		{
			getAll: {
				method: 'GET'
			},
			create: {
				method: 'POST'
			},
			update: {
				method: 'PUT'
			},
			delete: {
				method: 'DELETE'
			},
			associateClients: {
				method: 'POST',
				url: API_AGUAGENTE.URL+'/groups/:id_groups/associateClients'
			}

		}
	);

	return  {

    /**
     * getAll
     * Devuelve todos lo grupos.
     */
		getAll: function() {

			return resource
				.getAll()
				.$promise
				.then(function (response) {

					var groups = response.response;

					for(var i in groups) {

						groups[i].deposit = parseFloat(groups[i].deposit) / 100;
						groups[i].installation_fee = parseFloat(groups[i].installation_fee) / 100;
						groups[i].monthly_fee = parseFloat(groups[i].monthly_fee) / 100;
						groups[i].trial_days_price = parseFloat(groups[i].trial_days_price) / 100;
						groups[i].trial_days = parseInt(groups[i].trial_days);

					}


					return groups;

				});

		},

    /**
     * create
     * Crea un nuevo grupo.
     * @param payload Objeto que contiene información relacionada al nuevo grupo.
     */
		create: function(payload) {

			return resource
				.create((function(payload) {

					payload.deposit = parseFloat(payload.deposit) * 100;
					payload.installation_fee = parseFloat(payload.installation_fee) * 100;
					payload.monthly_fee = parseFloat(payload.monthly_fee) * 100;
					payload.trial_days_price = parseFloat(payload.trial_days_price) * 100;
					payload.trial_days = parseInt(payload.trial_days);

					return payload;

				})(payload))
				.$promise
				.then(function (response) {

					var response = response.response;

					response.deposit = parseFloat(response.deposit) / 100;
					response.installation_fee = parseFloat(response.installation_fee) / 100;
					response.monthly_fee = parseFloat(response.monthly_fee) / 100;
					response.trial_days_price = parseFloat(response.trial_days_price) / 100;
					response.trial_days = parseInt(response.trial_days);

					return response;

				});

		},

    /**
     * update
     * Actualiza la información algún grupo.
     * @param payload objeto que contiene la información del grupo a cambiar.
     */
		update: function(payload) {

			return resource
				.update((function(payload) {

					payload.deposit = parseFloat(payload.deposit) * 100;
					payload.installation_fee = parseFloat(payload.installation_fee) * 100;
					payload.monthly_fee = parseFloat(payload.monthly_fee) * 100;
					payload.trial_days_price = parseFloat(payload.trial_days_price) * 100;
					payload.trial_days = parseInt(payload.trial_days);

					return payload;

				})(payload))
				.$promise
				.then(function (response) {

					var response = response.response;

					response.deposit = parseFloat(response.deposit) / 100;
					response.installation_fee = parseFloat(response.installation_fee) / 100;
					response.monthly_fee = parseFloat(response.monthly_fee) / 100;
					response.trial_days_price = parseFloat(response.trial_days_price) / 100;
					response.trial_days = parseInt(response.trial_days);

					return response;

				});

		},

    /**
     * delete
     * Elimina algún grupo de base de datos y de KONECTA.
     * @param params Objeto con ID de grupo a eliminar.
     */
		delete: function(params) {

			return resource
				.delete(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * associateClients
     * Relaciona a un grupo con un conjunto de cliente. Guarda la información en base de datos.
     * @param payload Array con ID de clientes a asociar.
     */
		associateClients: function(payload) {

			return resource
				.associateClients(payload)
				.$promise
				.then(function (response) {

					return response.response;

				});

		}

	};

}

Groups.$inject = ['$resource', '$q', 'API_AGUAGENTE'];
