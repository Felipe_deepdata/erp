/**
 * WithDrawalsService
 * Modela la interacción de los retiros.
 *
 *
 * @param $http  ng/http
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, create: create}}
 * @constructor
 */
function WithdrawalsService ($http, API_AGUAGENTE) {

  return  {

    /**
     * getAll
     * Devuelve todos los retiros usando filtros.
     *
     * @param params
     */
		getAll: function(params) {

			return $http
				.get(API_AGUAGENTE.URL + '/withdrawals', {'params': params})
				.then(function(response) {
          var withdrawals = response.data.response;
					for(var i in withdrawals) {
						withdrawals[i].amount = parseFloat(withdrawals[i].amount);
					}
					return withdrawals;
				});

		},

    /**
     * create
     * Crea un retiro
     *
     * @param withdrawal
     */
    create: function(withdrawal) {

			return $http
				.post(API_AGUAGENTE.URL + '/withdrawals', withdrawal)
				.then(function(response) {
					var withdrawal = response.data.response;
					withdrawal.amount = parseFloat(withdrawal.amount);
					return withdrawal;
				});

		}

	};

}

WithdrawalsService.$inject = ['$http', 'API_AGUAGENTE'];
