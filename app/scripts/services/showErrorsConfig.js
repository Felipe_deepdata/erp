/**
 * showErrorsConfig
 * Configuración que establece el status del mensaje de error.
 */
function showErrorsConfig() {

	var _showSuccess;
    _showSuccess = false;

  /**
   * showSucces
   * Establece y devuelve la configuración del mensaje de error.
   *
   * @param showSuccess {true | false}
   * @returns {*}
   */
    this.showSuccess = function (showSuccess) {
      return _showSuccess = showSuccess;
    };

  /**
   * $get
   * Devuelve la configuración del mensaje de error.
   *
   * @returns {{showSuccess: boolean}}
   */
  this.$get = function () {
      return { showSuccess: _showSuccess };
    };

}
