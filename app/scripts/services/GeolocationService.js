/**
 * Geolocation
 * [Servicio de geolocalización de Google](https://developers.google.com/maps/documentation/geolocation/intro?hl=es-419)
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param AGUAGENTE_GOOGLE_API_KEY Key de acceso a API de Geolocalización de Google
 * @returns {{getCurrentPosition: getCurrentPosition}}
 * @constructor
 */
function Geolocation ($resource, AGUAGENTE_GOOGLE_API_KEY) {

	var resource = $resource(
		'https://www.googleapis.com/geolocation/v1/geolocate',
		{
		},
		{
			getCurrentPosition: {
				method: 'POST',
				params: {
					key: 'AIzaSyBg8jexNOc-plqDa4f2Au8nUKm--r3Alvk'
				}
			}
		}
	);

	return  {

    /**
     * getCurrentPosition
     * Devuelve la posición actual del cliente.
     * @param payload
     */
		getCurrentPosition: function (payload) {

			return resource
				.getCurrentPosition(payload)
				.$promise
				.then(function (response) {
					return {
						'coords': {
							'latitude': response.location.lat,
							'longitude': response.location.lng,
						}
					}
				});

		}

	}

}

Geolocation.$inject = ['$resource', 'AGUAGENTE_GOOGLE_API_KEY'];
